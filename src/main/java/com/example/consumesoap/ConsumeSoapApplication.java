package com.example.consumesoap;

import com.example.consumesoap.calculator.CalculatorClient;
import com.example.consumesoap.wsdl.Add;
import com.example.consumesoap.wsdl.AddResponse;
import com.example.consumesoap.wsdl.SubtractResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ConsumeSoapApplication {

	@Bean
	CommandLineRunner lookup(CalculatorClient calculatorClient) {
		return (String... args) -> {

			int addResult = 0 ;

			AddResponse response = calculatorClient.getAddResponse(addResult);
			System.err.println("the add result is " +response.getAddResult());

			int subResult = 0 ;

			SubtractResponse response2 = calculatorClient.getSubtractResponse(subResult);
			System.err.println("the subtract result is " +response2.getSubtractResult());

		};
	}


	public static void main(String[] args) {
		SpringApplication.run(ConsumeSoapApplication.class, args);
	}
}
