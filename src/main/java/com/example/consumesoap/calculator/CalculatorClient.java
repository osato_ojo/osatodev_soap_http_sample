package com.example.consumesoap.calculator;

import com.example.consumesoap.wsdl.Add;
import com.example.consumesoap.wsdl.AddResponse;
import com.example.consumesoap.wsdl.Subtract;
import com.example.consumesoap.wsdl.SubtractResponse;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * @Author: Ojo Osato LeToya
 * @Email: osato.ojo@live.com, osato.ojo@cwg-plc.com
 * @Created: 26/11/2018
 */
public class CalculatorClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(CalculatorClient.class);


    public AddResponse getAddResponse (int addResult){

        Add request = new Add();

        request.setIntA(7);
        request.setIntB(11);

        log.info("received first number " +request.getIntA() );
        log.info("received second number " + request.getIntB() );


        AddResponse response = (AddResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://www.dneonline.com/calculator.asmx?WSDL", request,
                        new SoapActionCallback(
                                "http://tempuri.org/Add"));


        return response;
    }

    public SubtractResponse getSubtractResponse (int subtractResult){

        Subtract request = new Subtract();

        request.setIntA(7);
        request.setIntB(11);

        log.info("received first number " +request.getIntA() );
        log.info("received second number " + request.getIntB() );


        SubtractResponse response = (SubtractResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://www.dneonline.com/calculator.asmx?WSDL", request,
                        new SoapActionCallback(
                                "http://tempuri.org/Subtract"));


        return response;
    }

//    public AddResponse getRESTAPI() throws Exception
//    {
//        DefaultHttpClient httpClient = new DefaultHttpClient();
//        try
//        {
//            //Define a HttpGet request; You can choose between HttpPost, HttpDelete or HttpPut also.
//            //Choice depends on type of method you will be invoking.
//            HttpGet getRequest = new HttpGet("http://localhost:8080/RESTfulDemoApplication/user-management/users/10");
//
//            //Set the API media type in http accept header
//            getRequest.addHeader("accept", "application/xml");
//
//            //Send the request; It will immediately return the response in HttpResponse object
//            HttpResponse response = httpClient.execute(getRequest);
//
//            //verify the valid error code first
//            int statusCode = response.getStatusLine().getStatusCode();
//            if (statusCode != 200)
//            {
//                throw new RuntimeException("Failed with HTTP error code : " + statusCode);
//            }
//
//            //Now pull back the response object
//            HttpEntity httpEntity = response.getEntity();
//            String apiOutput = EntityUtils.toString(httpEntity);
//
//            //Lets see what we got from API
//            System.out.println(apiOutput); //<user id="10"><firstName>demo</firstName><lastName>user</lastName></user>
//
//            //In realtime programming, you will need to convert this http response to some java object to re-use it.
//            //Lets see how to jaxb unmarshal the api response content
//            JAXBContext jaxbContext = JAXBContext.newInstance(User.class);
//            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//            User user = (User) jaxbUnmarshaller.unmarshal(new StringReader(apiOutput));
//
//            //Verify the populated object
//            System.out.println(user.getId());
//            System.out.println(user.getFirstName());
//            System.out.println(user.getLastName());
//        }
//        finally
//        {
//            //Important: Close the connect
//            httpClient.getConnectionManager().shutdown();
//        }
//        return
//    }

}
